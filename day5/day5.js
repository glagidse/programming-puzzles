const fs = require('fs');

let problemInput = fs.readFileSync('./day5/input.txt', 'utf8').trim();

const naughtyParts = ['ab', 'cd', 'pq', 'xy']
const vowels = ['a', 'e', 'i', 'o', 'u']

function isNiceV1(str) {
    const strChars = str.split('')

    let vowelCount = 0
    let hasTwoCharsInaRow = false
    let hasNaughtyParts = false

    let lastChar = null
    for(let i = 0; i < strChars.length; i++) {
        let currChar = strChars[i]
        if(vowels.includes(currChar))
            vowelCount++

        if(currChar == lastChar)
            hasTwoCharsInaRow = true
        
        if(naughtyParts.includes(lastChar + currChar)) {
            hasNaughtyParts = true
            break
        }

        lastChar = currChar
    }

    return (vowelCount >= 3) &&
        hasTwoCharsInaRow &&
        !hasNaughtyParts
}



function isNiceV2(str) {
    const strChars = str.split('')

    let hasPairThatAppearsTwice = false
    let hasXYXpattern = false

    for(let i = 0; i < strChars.length; i++) {
        let prev2 = i >= 2 ? strChars[i - 2] : null
        let prev1 = i >= 1 ? strChars[i - 1] : null
        let curr = strChars[i]

        if(prev1) {
            let currPart = prev1 + curr
            let check = str.indexOf(currPart)

            if (str.includes(currPart, check + 2))
                hasPairThatAppearsTwice = true
        }

        if(prev2 && prev1) {
            if(prev2 == curr)
                hasXYXpattern = true
        }
    }

    return hasPairThatAppearsTwice &&
        hasXYXpattern
}

function part1(input = problemInput){
    return input.split('\n')
        .reduce((acc, val) => acc +
            (isNiceV1(val) ? 1 : 0), 0)
}

function part2(input = problemInput){
    return input.split('\n')
        .reduce((acc, val) => acc +
            (isNiceV2(val) ? 1 : 0), 0)
}

module.exports = {
    part1: part1,
    part2: part2
}