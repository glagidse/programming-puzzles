const day = require('./day5')

test('d5p1', () => { expect(day.part1('ugknbfddgicrmopn'))
    .toBe(1) })
test('d5p1', () => { expect(day.part1('aaa'))
    .toBe(1) })
test('d5p1', () => { expect(day.part1('jchzalrnumimnmhp'))
    .toBe(0) })
test('d5p1', () => { expect(day.part1('haegwjzuvuyypxyu'))
    .toBe(0) })
test('d5p1', () => { expect(day.part1('dvszwmarrgswjxmb'))
    .toBe(0) })
test('d5p1', () => { expect(day.part1(
`dvszwmarrgswjxmb
haegwjzuvuyypxyu
ugknbfddgicrmopn
aaa`
))
    .toBe(2) })

test('d5p2', () => { expect(day.part2('qjhvhtzxzqqjkmpb'))
    .toBe(1) })
test('d5p2', () => { expect(day.part2('xxyxx'))
    .toBe(1) })
test('d5p2', () => { expect(day.part2('uurcxstgmygtbstg'))
    .toBe(0) })
test('d5p2', () => { expect(day.part2('ieodomkazucvgmuy'))
    .toBe(0) })
test('d5p2', () => { expect(day.part2(
`qjhvhtzxzqqjkmpb
uurcxstgmygtbstg
xxyxx
ieodomkazucvgmuy`
))
    .toBe(2) })
test('d5p2', () => { expect(day.part2('xyxxx'))
    .toBe(0) })
test('d5p2', () => { expect(day.part2('xaaaxa'))
    .toBe(1) })
test('d5p2', () => { expect(day.part2(
`bpmkjqqzxzthvhjq
uurcxstgmygtbstg
xxyxx
ieodomkazucvgmuy`
    ))
        .toBe(2) })
test('d5p2', () => { expect(day.part2('xyzzzxy'))
        .toBe(1) })