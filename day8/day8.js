const fs = require('fs');

const problemInput = fs.readFileSync('./day8/input.txt', 'utf8').trim();

function calculateTextDifference(line) {
    const codeLength = line.length
    let text = line.replace(/\\"|\\x..|\\\\/g, "X")
        text = text.substring(1, text.length -1)
    const textLength = text.length
    return codeLength - textLength
}

function calculateEncodedDifference(line) {
    const codeLength = line.length
    let text = line.replace(/\"|\\/g, "\\\"")
        text = "\"" + text + "\""

    const encodedLength = text.length
    return encodedLength - codeLength
}

function part1(input = problemInput){

    const lines = input.split('\n')
    let totalLength = 0
    for(let i = 0; i < lines.length; i++) {
        totalLength += calculateTextDifference(lines[i])
    }
    return totalLength
}

function part2(input = problemInput){
    const lines = input.split('\n')
    let totalLength = 0
    for(let i = 0; i < lines.length; i++) {
        totalLength += calculateEncodedDifference(lines[i])
    }
    return totalLength
}

module.exports = {
    part1: part1,
    part2: part2
}