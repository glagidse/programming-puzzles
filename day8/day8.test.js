const day = require('./day8')

const input1 =
`""
"abc"
"aaa\\"aaa"
"\\x27"`

test('d7p1', () =>{ expect(day.part1(
    `""`)).toBe(2) })
test('d7p1', () =>{ expect(day.part1(
    `"abc"`)).toBe(2) })
test('d7p1', () =>{ expect(day.part1(
    `"aaa\\"aaa"`)).toBe(3) })
test('d7p1', () =>{ expect(day.part1(
    `"\\x27"`)).toBe(5) })
test('d7p1', () =>{ expect(day.part1(
    `"\\\\"`)).toBe(3) })
test('d7p1', () =>{ expect(day.part1(
    `"daz\\\\zyyxddpwk"`)).toBe(3) })
test('d7p1', () =>{ expect(day.part1(
    input1)).toBe(12) })

test('d7p2', () =>{ expect(day.part2(
    `""`)).toBe(4) })
test('d7p2', () =>{ expect(day.part2(
    `"abc"`)).toBe(4) })
test('d7p2', () =>{ expect(day.part2(
    `"aaa\\"aaa"`)).toBe(6) })
test('d7p2', () =>{ expect(day.part2(
    `"\\x27"`)).toBe(5) })
test('d7p2', () =>{ expect(day.part2(
    input1)).toBe(19) })