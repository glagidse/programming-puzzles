function runProblem(problemNumber) {
    const path = `./day${problemNumber}/day${problemNumber}`
    const day = require(path)
    console.log(`problem ${problemNumber}`)
    console.log("part1:" + day.part1())
    console.log("part2:" + day.part2())
}

// runProblem(1)
// runProblem(2)
// runProblem(3)
// runProblem(4)
// runProblem(5)
// runProblem(6)
// runProblem(7)
runProblem(8)