const problemInput = 'bgvyzdsv'
const crypto = require('crypto');

function findAdventCoin(input, startsWith) {
    let index = 1
    while(true) {
        const key = input + index
        const result = crypto.createHash('md5')
            .update(key).digest("hex");
        if(result.startsWith(startsWith)) {
            return index
        }
        index++
    }
}

function part1(input = problemInput){
    return findAdventCoin(input, '00000')
}

function part2(input = problemInput){
    return findAdventCoin(input, '000000')
}

module.exports = {
    part1: part1,
    part2: part2
}