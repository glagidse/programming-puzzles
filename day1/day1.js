const fs = require('fs');

let problemInput = fs.readFileSync('./day1/input.txt', 'utf8');

function part1(input = problemInput) {
    return input.split('')
        .reduce( ((acc, val) => acc + (val == '(' ? 1 : -1)), 0)
}

function part2(input = problemInput) {
    let level = 0
    for(let i = 0; i < input.length; i++){
        if(input.charAt(i) == '(')
            level++
        else if(input.charAt(i) == ')')
            level--

        if(level == -1)
            return (i + 1)
    }
    return -1
}

module.exports = {
    part1: part1,
    part2: part2
}