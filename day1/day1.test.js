const floor1 = require('./day1.js')

test('d1p1', () => { expect(floor1.part1('(())')).toBe(0) })
test('d1p1', () => { expect(floor1.part1('()()')).toBe(0) })
test('d1p1', () => { expect(floor1.part1('(((')).toBe(3) })
test('d1p1', () => { expect(floor1.part1('(()(()(')).toBe(3) })
test('d1p1', () => { expect(floor1.part1('))(((((')).toBe(3) })
test('d1p1', () => { expect(floor1.part1('())')).toBe(-1) })
test('d1p1', () => { expect(floor1.part1('))(')).toBe(-1) })
test('d1p1', () => { expect(floor1.part1(')))')).toBe(-3) })
test('d1p1', () => { expect(floor1.part1(')())())')).toBe(-3) })

test('d1p2', () => { expect(floor1.part2(')')).toBe(1) })
test('d1p2', () => { expect(floor1.part2('()())')).toBe(5) })
test('d1p2', () => { expect(floor1.part2('()()))))')).toBe(5) })


