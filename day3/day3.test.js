const day = require('./day3')

test('d3p1', () => { expect(day.part1('>')).toBe(2) })
test('d3p1', () => { expect(day.part1('^>v<')).toBe(4) })
test('d3p1', () => { expect(day.part1('^v^v^v^v^v')).toBe(2) })

test('d3p2', () => { expect(day.part2('^v')).toBe(3) })
test('d3p2', () => { expect(day.part2('^>v<')).toBe(3) })
test('d3p2', () => { expect(day.part2('^v^v^v^v^v')).toBe(11) })