const fs = require('fs');

let problemInput = fs.readFileSync('./day3/input.txt', 'utf8').trim();

function getKey(posX, posY) {
    return `${posX}_${posY}`
}

function addVisited(visited, posX, posY) {
    const key = getKey(posX, posY)
    const currValue = visited[key] ? visited[key] : 0
    visited[key] = currValue + 1
}

function getNextPosition(posX, posY, update) {
    const retVal = {
        posX: posX,
        posY: posY
    }

    switch(update) {
        case '>':
            retVal.posX++
            break;
        case '<':
            retVal.posX--
            break;
        case '^':
            retVal.posY--
            break;
        case 'v':
            retVal.posY++
            break;
    }

    return retVal
}

function updateVisitedLocations(directions, visited){
    let posX = 0
    let posY = 0

    directions.forEach(x => {
        const nextPos = getNextPosition(posX, posY, x)
        posX = nextPos.posX
        posY = nextPos.posY
        addVisited(visited, posX, posY)
    });

    return visited
}

function part1(input = problemInput){
    const visited = {}
    visited[getKey(0, 0)] = 1
    const total = updateVisitedLocations(input.split(''),
        visited)

    return Object.keys(total).length
}

function part2(input = problemInput){
    const visited = {}
    visited[getKey(0, 0)] = 2
    const movements = input.split('')
    const santaMovements = movements.filter((v, i) => !(i % 2))
    const roboMovements = movements.filter((v, i) => i % 2)

    updateVisitedLocations(santaMovements, visited)
    updateVisitedLocations(roboMovements, visited)

    return Object.keys(visited).length
}

module.exports = {
    part1: part1,
    part2: part2
}