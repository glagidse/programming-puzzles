const day = require('./day6')

test('d6p1', () =>{ expect(day.part1(
    `turn on 0,0 through 9,9`))
        .toBe(100) })
test('d6p1', () =>{ expect(day.part1(
`turn on 0,0 through 9,9
turn on 10,10 through 19,19`))
        .toBe(200) })
test('d6p1', () =>{ expect(day.part1(
`turn on 0,0 through 999,999`))
    .toBe(1000000) })
test('d6p1', () =>{ expect(day.part1(
`toggle 0,0 through 999,0`))
    .toBe(1000) })
test('d6p1', () =>{ expect(day.part1(
`turn off 499,499 through 500,500`))
    .toBe(0) })

    

test('d6p2', () =>{ expect(day.part2(
    `turn on 0,0 through 0,0`))
        .toBe(1) })
test('d6p2', () =>{ expect(day.part2(
    `toggle 0,0 through 999,999`))
        .toBe(2000000) })
