const fs = require('fs');

let problemInput = fs.readFileSync('./day6/input.txt', 'utf8').trim();

function createGrid(width, height) {
    return Array(height).fill([], 0, height)
        .map( x =>
            Array(width).fill(0, 0, width), 0, height
        )
}

function countLightsOn(grid) {
    let reducer2 = (acc, val) => acc + val
    let reducer1 = (acc, val) => acc +
        val.reduce(reducer2, 0)
    return grid.reduce(reducer1 ,0)
}

function parseInstruction(instruction, funcs) {
    let inputParts = instruction.split(' ')

    let startPositions = inputParts[
        inputParts.length - 3
    ].split(',')
    let endPositions = inputParts[
        inputParts.length - 1
    ].split(',')

    let type =
        inputParts[0] == 'toggle' ? funcs.toggle :
        inputParts[1] == 'on' ? funcs.on :
        funcs.off

    return {
        type: type,
        startX: parseInt(startPositions[0]),
        startY: parseInt(startPositions[1]),
        endX: parseInt(endPositions[0]),
        endY: parseInt(endPositions[1])
    }
}



function executeInstruction(grid, instr) {
    for(let i = instr.startY; i <= instr.endY; i++) {
        for(let j = instr.startX; j <= instr.endX; j++) {
            grid[i][j] = instr.type(grid[i][j])
        }
    }
}

function part1(input = problemInput){
    let grid = createGrid(1000, 1000)

    let part1funcs = {
        on: () => 1,
        off: () => 0,
        toggle: (x) => x == 0 ? 1 : 0
    }

    input.split('\n').forEach(x => {
        executeInstruction(grid,
            parseInstruction(x,
                part1funcs))
    });
    return countLightsOn(grid)
}

function part2(input = problemInput){
    let grid = createGrid(1000, 1000)

    let part1funcs = {
        on: (x) => x + 1,
        off: (x) => x > 0 ? x - 1: 0,
        toggle: (x) => x + 2
    }

    input.split('\n').forEach(x => {
        executeInstruction(grid,
            parseInstruction(x,
                part1funcs))
    });
    return countLightsOn(grid)
}

module.exports = {
    part1: part1,
    part2: part2
}