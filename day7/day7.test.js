const day = require('./day7')

const input1 =
`123 -> x
456 -> y`

const input2 =
`123 -> x
456 -> y
x AND y -> d
x OR y -> e
x LSHIFT 2 -> f
y RSHIFT 2 -> g
NOT x -> h
NOT y -> i`

const input3 =
`y RSHIFT 2 -> g
x AND y -> d
x AND 456 -> k
k -> i
123 -> x
456 -> y`

test('d7p1', () =>{ expect(day.part1(
    '123 -> x', 'x')).toBe(123) })
test('d7p1', () =>{ expect(day.part1(
    '456 -> y', 'y')).toBe(456) })
test('d7p1', () =>{ expect(day.part1(
    input1, 'x')).toBe(123)})
test('d7p1', () =>{ expect(day.part1(
    input1, 'y')).toBe(456)})
test('d7p1', () =>{ expect(day.part1(
    input2, 'x')).toBe(123)})
test('d7p1', () =>{ expect(day.part1(
    input2, 'x')).toBe(123)})
test('d7p1', () =>{ expect(day.part1(
    input2, 'd')).toBe(72)})
test('d7p1', () =>{ expect(day.part1(
    input2, 'e')).toBe(507)})
test('d7p1', () =>{ expect(day.part1(
    input2, 'f')).toBe(492)})
test('d7p1', () =>{ expect(day.part1(
    input2, 'g')).toBe(114)})
test('d7p1', () =>{ expect(day.part1(
    input2, 'h')).toBe(65412)})
test('d7p1', () =>{ expect(day.part1(
    input2, 'i')).toBe(65079)})
test('d7p1', () =>{ expect(day.part1(
    input3, 'x')).toBe(123)})
test('d7p1', () =>{ expect(day.part1(
    input3, 'x')).toBe(123)})
test('d7p1', () =>{ expect(day.part1(
    input3, 'd')).toBe(72)})
test('d7p1', () =>{ expect(day.part1(
    input3, 'g')).toBe(114)})
test('d7p1', () =>{ expect(day.part1(
    input3, 'k')).toBe(72)})
test('d7p1', () =>{ expect(day.part1(
    input3, 'i')).toBe(72)})

test('d7p2', () =>{ expect(day.part2(
    input1, 'y', 'x')).toBe(456)})