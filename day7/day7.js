const fs = require('fs');

let problemInput = fs.readFileSync('./day7/input.txt', 'utf8').trim();

function getMapping(name, mapping, cache) {
    if(!isNaN(parseInt(name))){
        return parseInt(name)
    } else if(cache[name]) {
        return cache[name]
    } else {
        cache[name] = calculateExpression(name, mapping, cache)
        return cache[name]
    }
}

function calculateExpression(name, mapping, cache = {}) {
    const expression = mapping[name]
    const expressionParts = expression.split(' ')
    let result = null

    if(expressionParts.length === 1) {
        result = getMapping(expressionParts[0], mapping, cache)
    } else if (expressionParts.length === 2){
        result = 65535 - getMapping(expressionParts[1], mapping, cache)
    } else if (expressionParts.length === 3) {
        if(expressionParts[1] === "AND")
            result = getMapping(expressionParts[0], mapping, cache) &
                getMapping(expressionParts[2], mapping, cache)
        else if (expressionParts[1] === "OR")
            result = getMapping(expressionParts[0], mapping, cache) |
                getMapping(expressionParts[2], mapping, cache)
        else if (expressionParts[1] === "LSHIFT")
            result = getMapping(expressionParts[0], mapping, cache) << expressionParts[2]
        else if (expressionParts[1] === "RSHIFT")
            result = getMapping(expressionParts[0], mapping, cache) >> expressionParts[2]
    }

    return result
}

function getExpressionVariableMapping(lines){
    const mapping = {}
    for(let i = 0; i < lines.length; i++){
        const parts = lines[i].split(' -> ')
        const variable = parts[1]
        const expression = parts[0]
        mapping[variable] = expression
    }
    return mapping
}

function part1(input = problemInput, target = 'a'){

    const lines = input.split('\n')
    const mapping = getExpressionVariableMapping(lines)
    const result =  calculateExpression(target, mapping)

    return result;
}

function part2(input = problemInput, target = 'a', override = 'b'){
    const lines = input.split('\n')
    const mapping = getExpressionVariableMapping(lines)
    const result =  calculateExpression(target, mapping)
    mapping[override] = result.toString()
    const newResult = calculateExpression(target, mapping)
    return newResult
}

module.exports = {
    part1: part1,
    part2: part2
}