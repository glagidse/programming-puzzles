const fs = require('fs');

let problemInput = fs.readFileSync('./day2/input.txt', 'utf8').trim();

function splitByNewLine(input) {
    return input.split('\n')
}

function getDimensions(boxInfo) {
    return boxInfo.split('x').map((x) => Number(x))
}

function getSides(dimensions) {
    return [
        dimensions[0] * dimensions[1],
        dimensions[1] * dimensions[2],
        dimensions[2] * dimensions[0]
    ]
}

function getArea(box) {
    const dimensions = getDimensions(box)
    const sides = getSides(dimensions)
    sides.sort((l, r) => l > r)
    return 3 * sides[0] + 2 * sides[1] + 2 * sides[2]
}

function getRibbonSizeNeeded(boxInfo) {
    const dimensions = getDimensions(boxInfo)
    dimensions.sort((l, r) => l > r)

    return dimensions[0] * 2 + dimensions[1] * 2 +
    dimensions[0] * dimensions[1] * dimensions[2]
}

function part1(input = problemInput){
    const boxes = splitByNewLine(input)
    const result = boxes.reduce((acc, val) => acc + getArea(val), 0) 
    return result
}

function part2(input = problemInput){
    const boxes = splitByNewLine(input)
    return boxes.reduce((acc, val) => acc + getRibbonSizeNeeded(val), 0) 
}

module.exports = {
    part1: part1,
    part2: part2
}