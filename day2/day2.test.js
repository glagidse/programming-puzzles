const day2 = require('./day2')

test('d2p1', () => { expect(day2.part1('2x3x4')).toBe(58) })
test('d2p1', () => { expect(day2.part1('1x1x10')).toBe(43) })
test('d2p1', () => { expect(day2.part1(
`2x3x4\n1x1x10`)).toBe(101) })
test('d2p1', () => { expect(day2.part1(`2x3x4\n1x1x10
2x3x4\n1x1x10`)).toBe(202) })


test('d2p2', () => { expect(day2.part2('2x3x4')).toBe(34) })
test('d2p2', () => { expect(day2.part2('1x1x10')).toBe(14) })
test('d2p2', () => { expect(day2.part2('2x3x4\n1x1x10')).toBe(48) })
test('d2p2', () => { expect(day2.part2('2x3x4\n1x1x10\n2x3x4\n1x1x10')).toBe(96) })
test('d2p2', () => { expect(day2.part2('3x11x24')).toBe(820) })